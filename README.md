
# ⚠ <a href="https://gitlab.opencode.de/bmi/eudi-wallet">Wir sind umgezogen</a>  ⚠

# ⚠ <a href="https://gitlab.opencode.de/bmi/eudi-wallet">We moved</a>  ⚠


Zur besseren Übersicht über Einreichungen und Issues haben wir die Teilprojekte des Architektur- und Konsultationsprozesses in einer Gruppe vereinigt.
Sie finden Ihre Beiträge und Issues wie gewohnt an neuer Stelle. 

For a better overview of submissions and issues, we have merged the sub-projects of the architecture and consultation process into one group.
As usual, you will find your contributions and issues in a new place. 